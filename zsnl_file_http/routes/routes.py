# AUTO GENERATED FILE - don't edit!
# Re-create by running 'generate-routes --package-name={package_name}'
# Run 'generate-routes --help' for more options


def add_routes(config):
    """Add routes to pyramid application.

    :param config: pyramid config file
    :type config: Configurator
    """
    handlers = [
        {
            "route": "/hello", 
            "handler": "hello_world",
            "method": "GET",
            "view": "zsnl_file_http.views.hello.hello_world",
        },
        {
            "route": "/set_hello", 
            "handler": "hello_save",
            "method": "POST",
            "view": "zsnl_file_http.views.hello.hello_save",
        },
    ]

    for h in handlers:
        config.add_route(h["handler"], h["route"])
        config.add_view(
            view=h["view"],
            route_name=h["handler"],
            renderer="json",
            request_method=h["method"],
        )
