def hello_world(request):
    """Say hello."""
    query = request.get_query_instance(domain="zsnl_file_http.domain.hello")
    return query.hello_query(uuid=request.GET["uuid"])


def hello_save(request):
    """Save a greeting"""

    cmd = request.get_command_instance(domain="zsnl_file_http.domain.hello")
    cmd.set_hello_name(uuid=request.POST["uuid"], name=request.POST["name"])

    return {"success": True}
