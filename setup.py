#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import find_packages, setup

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.md") as history_file:
    history = history_file.read()

with open("requirements/base.txt") as f:
    requirements = f.read().splitlines()
    dependency_links = []
    for req in requirements:
        if req.startswith("git+"):
            requirements.remove(req)
            dependency_links.append(req)

with open("requirements/test.txt") as f:
    test_requirements = f.read().splitlines()

setup(
    author="Martijn van de Streek",
    author_email="martijn@mintlab.nl",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="HTTP server for the file domain",
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    dependency_links=dependency_links,
    license="EUPL license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="zsnl_file_http",
    name="zsnl_file_http",
    packages=find_packages(include=["zsnl_file_http", "zsnl_file_http.*"]),
    package_data={"": ["*.json"]},
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/minty-python/zsnl-file-http",
    version="0.0.1",
    zip_safe=False,
    entry_points={"paste.app_factory": ["main = zsnl_file_http:main"]},
)
