#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

mkdir -p /tmp/tests && pytest --junitxml=/tmp/tests/junit.xml --cov=zsnl_file_http --cov-report=term
